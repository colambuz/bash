#!/bin/bash

#Имя файлв
file=./access.log

#Количество всех строк в файле
allRows=$(wc $file | awk '{print $1}')

#Время начала файла
startDate=$(awk '{print $4}' $file | sed 's/\[//' | sed -n 1p)

#Врема конца файла
endDate=$(awk '{print $4}' $file | sed 's/\[//' |sort -r | sed -n 1p)

#IP адреса с наибольшим количеством запросов
ips=$(awk '{print $1}' $file | sort |  uniq -c | sort -nr | head)

#Запрашиваемые адреса наибольшее количество раз
adrs=$(awk '{print $11}' $file  | sed 's/"-"//g' | sed 's/uct=//g' | sed '/^[[:space:]]*$/d' | sort | uniq -c | sort -nr | head)

#Ошибки
err=$(cat $file | grep -i err | awk '{print $7}')
 
#Список всех кодов возврата
all=$(awk '{print $9}' $file | sed 's/"-"//g' | sed '/^[[:space:]]*$/d' | sort | uniq -c | sort -nr)

#Приоверка на мультизапуск скрипта
cnt=$(ps aux | grep -E '.*/script.sh$' | grep -v 'grep' | wc -l)
if [ $cnt -ne 2 ]; then
    echo "Already running" && exit 0
fi

#Отправка письма
echo -e "За период с $startDate по $endDate\n
10 IP адресов (с наибольшим кол-вом запросов):\n
$ips\n
10 запрашиваемых адресов (с наибольшим кол-вом запросов):\n
$adrs\n
Все ошибки c момента последнего запуска:\n
$err\n
Список всех кодов возврата с указанием их кол-ва:\n
$all" | mail -s "NGINX Log Info" your_user@localhost
